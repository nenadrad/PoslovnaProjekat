﻿using System.Web;
using System.Web.Optimization;

namespace Poslovna
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));
          
            bundles.Add(new ScriptBundle("~/bundles/angular")             
                    .Include( "~/Scripts/angular.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-ui-router")
                .Include("~/Scripts/angular-ui-router.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-ui-module")
                .Include("~/Scripts/angular-ui-module.js"));

            bundles.Add(new ScriptBundle("~/bundles/ngStorage").Include("~/Scripts/ngStorage.min.js"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/toastr").Include("~/Scripts/toastr.min.js"));
            bundles.Add(new StyleBundle("~/Content/toastr").Include("~/Content/toastr.min.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/spacelab.bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/contrl")
                    .Include("~/Scripts/mainApp.js")
                    .IncludeDirectory("~/Scripts/Controllers", "*.js")
                    .IncludeDirectory("~/Scripts/Services", "*.js")
                    .IncludeDirectory("~/Scripts/Moduls", "*.js"));



        }
    }
}
