﻿(function (angular) {

    var myController = function ($scope) {
        $scope.message = "Hello from the controller!";
        console.log("Usao");
       
    };
    var loginModule = angular.module("loginModule", []);
    var metadataModule = angular.module('metadataModule', []);
    var tableModule = angular.module('tableModule', []);
    var routingModule = angular.module('routingModule', []);
    var registerModule = angular.module('registerModule', []);
    var reportModule = angular.module('reportModule', []);
 
    var application = angular.module("app", ['tableModule','ui.bootstrap', 'metadataModule', 'ngStorage', 'routingModule', 'ui.router', 'loginModule', 'registerModule']);

   
})(angular);
