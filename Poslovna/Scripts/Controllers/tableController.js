﻿var tableController = function ($scope, tableFactory, metadataFactory) {

    $scope.getTableByCode = function(uri) {

        tableFactory.getTable(uri)
         .then(function (response) {
             $scope.tableData = response.data;
             console.log(response.data)
             console.log(uri);
         }, function (error) {
             $scope.status = 'Unable to load customer data: ' + error.message;
         });

    }

    $scope.getObjectValue = function (object, code) {
        return object[code];
    }

}
var tableModule = angular.module('tableModule');
tableModule.controller('tableController',  tableController);