﻿robnaKarticaReportController = function ($scope, $http, $sce) {

    var data = $scope.dataForReport;

    $scope.showButton = true;

    function authenticate() {

        var url = "http://localhost:8080/jasperserver/rest/login?j_username=jasperadmin&j_password=jasperadmin";

        return $http.get(url);
    }

    authenticate().then(function (response) {
        console.log("uspesno logovanje");
        $http.get("http://localhost:8080/jasperserver/rest_v2/reports/Reports/MagacinskaKartica.html?id_robe=" + data.ID_ROBE + "&ime_skladista=" + data.MAGACIN.NAZIV_MAGACINA + "&naziv_robe=" + data.ROBA.NAZIV_ROBE + "&id_kartice="+data.ID_KARTICE)
            .then(function (response) {
                console.log("uspesno ucitan izvestaj");
                $scope.reportHtml = $sce.trustAsHtml(response.data);
                console.log(response.data);
            }, function (error) {
                console.log(error);
            });

    }, function (error) {
        console.log(error);
    });

}


var app = angular.module('app');
app.controller('robnaKarticaReportController', ['$scope', '$http', '$sce', robnaKarticaReportController]);