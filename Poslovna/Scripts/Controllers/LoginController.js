﻿var loginController = function ($scope, $log, authenticationFactory, $uibModal) {
    $scope.login = function ()
    {
        authenticationFactory.login($scope.username, $scope.password, loginCbck);

        function loginCbck(success) {
            if (success) {
                $log.info('success!');
                poslovnaGodinaModal();
            }
            else {
                $log.info('failure!');
            }
        }
    }
    function poslovnaGodinaModal() {
        var modalInstance = $uibModal.open({
            templateUrl: 'home/poslovnaGodinaForm',
            controller: 'modalController',
            backdrop: 'static',
            keyboard: false
        });
    };


}

var application = angular.module('loginModule');
application.controller('loginController',loginController);

