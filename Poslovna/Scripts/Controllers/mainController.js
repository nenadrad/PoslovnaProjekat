﻿mainController = function ($rootScope, $state) {
    if ($rootScope.isLoggedIn() == false) {
        $state.go('login');
    }

}

var app = angular.module('app');
app.controller('mainController', ['$rootScope', '$state', mainController]);