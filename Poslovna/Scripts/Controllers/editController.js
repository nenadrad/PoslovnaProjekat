﻿editController = function ($scope, $state, $stateParams, $timeout, $rootScope, tableFactory) {

    var uri = $stateParams["tableToLoadUrl"];

    if ($stateParams["isBackFromZoom"] == true)
        $scope.editData = $stateParams["editData"];

    var data = $scope.editData;

    checkNext();

    function checkNext() {
        if ($scope.isNext) {
            $timeout(function () {
                var field = document.getElementById($scope.filter.id);
                field.disabled = true;
            }, 0);
            
        }
    }

    $scope.getValue = function (columnCode) {
        return data[columnCode];
    }

    $scope.getSelectedValue = function (columnCode) {
        var editData;
        if ($stateParams["isBackFromZoom"] == true)
            editData = $stateParams["editData"];
        else
            editData = data;

        console.log(editData);
        var value = editData[columnCode];
        console.log(value);
        var options = $scope.getSelectOptions(columnCode);
        console.log(options.length);
        for (var i = 0; i < options.length; i++) {
            if (options[i].Id == value) {
                console.log("true");
                return options[i];
            }
        }
    }

    $scope.getDateValue = function (columnCode) {
        var dateTime = data[columnCode];
        return new Date(dateTime);
    }

    $scope.submitFormEdit = function () {
        var data = {};
        var fields = document.getElementsByTagName('input');
        var selectFields = document.getElementsByTagName('select');
        for (var i = 0; i < fields.length; i++) {
            var key = fields[i].getAttribute('id');
            var value = fields[i].value;
            if (fields[i].type == "checkbox") {
                if (value == "on")
                    value = true;
                if (value == "off")
                    value = false;
            }
            if (value == "")
                value = null;
            data[key] = value;
        }
        for (var i = 0; i < selectFields.length; i++) {
            var key = selectFields[i].getAttribute('id');
            var value = selectFields[i].value;
            data[key] = value;
        }

        var key = $scope.PrimaryKey;
        var id = $scope.getObjectValue($scope.editData, key);

        data[key] = id;

        tableFactory.updateTable($scope.tableUri, id, data)
            .then(function (response) {
                var index;
                for (var i = 0; i < $scope.tableData.length; i++) {
                    var item = $scope.tableData[i];
                    if (item[key] == id) {
                        index = i;
                        break;
                    }
                }
                tableFactory.getItem(uri, id)
                    .then(function (response) {
                        console.log(response.data);
                        $scope.tableData.splice(index, 1, response.data);
                        toastr.success("Izmena uspešna!");
                        var beforeState = $state.current.params.parent;
                        $state.go(beforeState);
                    })
                
            }, function(error) {
                toastr.error("Izmena nije uspela. Greška: " + error.message);
            });


    }

    $scope.zoomFromEdit = function (columnCode) {
        var tableUri;
        for (var i = 0; i < $scope.zoomTables.length; i++)
            if ($scope.zoomTables[i].ColumnCode == columnCode) {
                tableUri = $scope.zoomTables[i].Table.Uri;
            }

        $state.params.isBackFromZoom = true;
        $state.params.editData = $scope.editData;

        var currentState = {
            name: $state.current.name,
            params: $state.params
        };

        console.log(currentState);

        $rootScope.statesChain.push(currentState);

        $state.go(tableUri, { isZoom: true, isNext: false, filter: null });
    }
}

var tableModule = angular.module('tableModule');
tableModule.controller('editController', ['$scope', '$state', '$stateParams', '$timeout', '$rootScope', 'tableFactory', editController]);