﻿var metadataController = function ($scope, metadataFactory,$rootScope,$localStorage) {

    getTableNames();

    function getTableNames() {
        metadataFactory.getTableNames()
            .then(function (response) {
                $scope.tables = response.data;
                console.log(response.data);
            }, function (error) {
                $scope.status = 'Unable to load table data: ' + error.message;
            });
    }

    
    
    $rootScope.godinaPoslovanja = $localStorage.godinaPoslovanja;
    $rootScope.godinaPoslovanjaID = $localStorage.godinaPoslovanjaID;
    $rootScope.godinaPoslovanjaZakljucena = $localStorage.godinaPoslovanjaZakljucena;
    /*
    $scope. getTableColumns=  function(id) {
        metadataFactory.getTableColums(id)
            .then(function (response) {
                $scope.colums = response.data;
                console.log(response.data);
            }, function (error) {
                $scope.status = 'Unable to load table data: ' + error.message;
            });
    }
    */
    $scope.chanegState = function (state) {
        console.log(state);
        $state.go(state);
    }

};
var metadataModule = angular.module('metadataModule');
metadataModule.controller('MetadataCntrl', metadataController);
