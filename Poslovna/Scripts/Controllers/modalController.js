﻿var modalController = function ($scope, $modalInstance, tableFactory, $rootScope, $localStorage, $stateParams) {
    $scope.ok = function () {
        var godina = $scope.selecteItem.split(',')[0];
        var id = $scope.selecteItem.split(',')[1];
        var zakljucena = $scope.selecteItem.split(',')[2];
      

        $localStorage.godinaPoslovanja = godina;
        $localStorage.godinaPoslovanjaID = id;
        $localStorage.godinaPoslovanjaZakljucena = zakljucena;

        $rootScope.godinaPoslovanja = godina;
        $rootScope.godinaPoslovanjaID = id;
        $rootScope.godinaPoslovanjaZakljucena = zakljucena;

        $modalInstance.close();
    };



    
    $scope.ObrisiStavku = function () {
        var uri = $stateParams["tableToLoadUrl"];

        console.log("Brisanje");
        tableFactory.deleteTable(uri, $rootScope.stavkaZaBrisanje)
                .then(function (response) {
                    $scope.status = 'Deleted item! Refreshing table data.';
                    for (var i = 0; i < $rootScope.tableData.length; i++) {
                        var item = $rootScope.tableData[i];
                    if ($scope.getObjectValue(item, $rootScope.key) == $rootScope.stavkaZaBrisanje) {
                        $rootScope.tableData.splice(i, 1);
                        break;
                       }
                     }
        toastr.success("Stavka uspešno obrisana. ");
        }, function (error) {
          $scope.status = error.status;
           toastr.error("Nije moguce obrisati element sa id-em : " + $rootScope.stavkaZaBrisanje);
           console.log($scope.status);
     });
            $modalInstance.close();
    };

    $scope.getObjectValue = function (object, code) {
        return object[code];
    };

    $scope.ponistiBrisanje = function () {
        console.log("Ponisti");
        $modalInstance.close();
    };



    getPoslovnaGodina();

    function getPoslovnaGodina() {
        tableFactory.getPoslovnaGodina()
         .then(function (response) {
             $scope.poslovneGodine = response.data;
         }, function (error) {
             $scope.status = 'Unable to load table data: ' + error.message;
         });

    }
}

var application = angular.module('loginModule');
application.controller('modalController', modalController);