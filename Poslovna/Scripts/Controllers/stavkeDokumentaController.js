﻿stavkeDokumentaController = function ($scope, $state, metadataFactory, tableFactory) {

    getTableColumns();
    getStavke();

    function getTableColumns() {
        metadataFactory.getTableColums("STAVKE_DOKUMENTA")
            .then(function (response) {
                $scope.stColumns = response.data;
                console.log(response.data);
            }, function (error) {
                $scope.status = 'Unable to load table data: ' + error.message;
            });
    }

    function getStavke() {
        tableFactory.getStavke($scope.dokument["ID_DOKUMENTA"])
            .then(function (response) {
                $scope.stavkeData = response.data;
                console.log(response.data)
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
    }

    $scope.editStavka = function(data) {
        console.log("edit stavka");
    };

    $scope.deleteStavka = function (data) {
        var key = $scope.stColumns[0].Code;
        var id = $scope.getObjectValue(data, key);

        if (confirm('Da li ste sigurni da želite da obrišete stavku za šifrom ' + id + '?')) {
            tableFactory.deleteTable("/StavkeDokumenta", id)
                .then(function (response) {
                    $scope.status = 'Deleted item! Refreshing table data.';
                    for (var i = 0; i < $scope.stavkeData.length; i++) {
                        var item = $scope.stavkeData[i];
                        if ($scope.getObjectValue(item, key) == id) {
                            $scope.stavkeData.splice(i, 1);
                            break;
                        }
                    }
                    toastr.success("Stavka uspešno obrisana. ");
                }, function (error) {
                    $scope.status = error.status;
                    toastr.error("Nije moguce obrisati element sa id-em : " + id);
                    console.log($scope.status);
                });
        }
    };

    

}

var tableModule = angular.module('tableModule');
tableModule.controller('stavkeDokumentaController', ['$scope', '$state','metadataFactory', 'tableFactory', stavkeDokumentaController]);