﻿var routingController = function ($scope, $state, $stateParams, $rootScope, $timeout, tableFactory, metadataFactory, $uibModal) {

    var code = $stateParams["tableCode"];
    var uri = $stateParams["tableToLoadUrl"];

    $scope.tableName = $stateParams["tableName"];
    $scope.tableCode = $stateParams["tableCode"];
    $scope.tableUri = uri;
    $scope.isNext = $stateParams["isNext"];
    $scope.filter = $stateParams["filter"];
    $scope.isZoom = $stateParams["isZoom"];
    $scope.godinaPoslovanjaID = $rootScope.godinaPoslovanjaID;

    console.log("isNext: "+ $scope.isNext);
    console.log($scope.filter);

    getTableColumns();
    getNextTables();
   
    var tempColums = [];
    var tempTableData = [];

    function getTableColumns() {
        metadataFactory.getTableColums(code)
            .then(function (response) {
                tempColums = response.data;
                getPrimaryKey();
                getZoomTables();
            }, function (error) {
                $scope.status = 'Unable to load table data: ' + error.message;
            });
    }

    function getNextTables() {
        metadataFactory.getNextTables(code)
            .then(function (response) {
                $scope.nextTables = response.data;
                console.log(response.data);
            }, function (error) {
                $scope.status = 'Unable to load table data: ' + error.message;
            });
    }

    function getZoomTables() {
        metadataFactory.getZoomTables(code)
            .then(function (response) {
                $scope.zoomTables = response.data;
                
                for(var i=0; i<tempColums.length; i++)
                    for (j = 0; j < $scope.zoomTables.length; j++) {
                        if ($scope.zoomTables[j].ColumnCode == tempColums[i].Code) {
                            tempColums[i].Name = $scope.zoomTables[j].Table.Name;
                            tempColums[i].IsFK = true;
                            break;
                        }
                    }
                $scope.colums = tempColums;
                getZoomValues();
            }, function (error) {
                $scope.status = 'Unable to load table data: ' + error.message;
            });
    }

    function getZoomValues() {
        $scope.zoomValues = {};
        for (var i = 0; i < $scope.zoomTables.length; i++) {
            var data = {
                TableCode: $scope.tableCode,
                FKModel: $scope.zoomTables[i]
            };
            console.log(data);
            var columnCode = $scope.zoomTables[i].ColumnCode;
            sendQuery(columnCode, data);
        }
        getTableData();
    }

    function sendQuery(colCode, data) {
        tableFactory.getZoomValues(data)
                .then(function (response) {
                    console.log(response.data);
                    var retVal = [];
                    for (var j = 0; j < response.data.length; j++)
                        retVal[response.data[j].Id] = response.data[j].Value;
                    $scope.zoomValues[colCode] = retVal;
                    console.log($scope.zoomValues);
                }, function (error) {
                    console.log(error.data);
                });
    }

    $scope.proveraGodine = function (a) {
        if (a == undefined)
            return true;
        else if (a == $rootScope.godinaPoslovanjaID)
            return true;
        else
            return false;
    }

    $scope.proveraGodineZakljucena = function () {

        if (code == 'STAVKE_DOKUMENTA' && $scope.isNext && $scope.filter.dokument["STATUS"] == 'F')
            return true;

        if (code == 'ROBNA_KARTICA' || code == 'ANALITIKA_MAGACINSKE_KARTICE' || code == 'STAVKE_DOKUMENTA')
            return false;

        if (code == 'PROMETNI_DOKUMENT') {
            if ($rootScope.godinaPoslovanjaZakljucena=='true') {
                return false;
            }
        }

        
              
        return true;
    }

    function getTableData() {
        tableFactory.getTable(uri)
            .then(function (response) {
                $scope.tableData = response.data;
                console.log(response.data);
            }, function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
            });
    }

    function getPrimaryKey() {
        for (var i = 0; i < tempColums.length; i++) {
            if (tempColums[i].IsPrimary) {
                $scope.PrimaryKey = tempColums[i].Code;
                tempColums.splice(i, 1);
                console.log("primary key: " + $scope.PrimaryKey);
                break;
            }
        }
    }

    $scope.edit = function (data) {
        $scope.editData = data;
        if ($state.current.name.includes('izmena')) {
            var fields = document.getElementsByTagName('input');
            for (var i = 0; i < fields.length; i++)
                fields[i].value = data[fields[i].getAttribute('id')];
        }
        else
            $state.go($state.current.name + '.izmena');
    }



    $scope.delete = function (data) {
        var key = $scope.PrimaryKey;

        var id = $scope.getObjectValue(data, key);
        $rootScope.key = key;
        $rootScope.stavkaZaBrisanje = id;
        $rootScope.tableData = $scope.tableData;

        var modalInstance = $uibModal.open({
            templateUrl: 'home/deleteForm',
            controller: 'modalController',
            backdrop: 'static',
            keyboard: true,
        });
        
        /*
        if (confirm('Da li ste sigurni da želite da obrišete stavku za šifrom ' + id + '?')) {
            tableFactory.deleteTable(uri, id)
                .then(function (response) {
                    $scope.status = 'Deleted item! Refreshing table data.';
                    for (var i = 0; i < $scope.tableData.length; i++) {
                        var item = $scope.tableData[i];
                        if ($scope.getObjectValue(item, key) == id) {
                            $scope.tableData.splice(i, 1);
                            break;
                        }
                    }
                    toastr.success("Stavka uspešno obrisana. ");
                }, function (error) {
                    $scope.status = error.status;
                    toastr.error("Nije moguce obrisati element sa id-em : " + id);
                    console.log($scope.status);
                });
        }*/
    };




    $scope.getObjectValue = function (object, code) {
        return object[code];
    };

    $scope.getTableValue = function (object, column) {
        if (column.IsFK) {
            var id = object[column.Code];
            return $scope.zoomValues[column.Code][id];
        }
        else {
            return object[column.Code];
        }
    }

    $scope.submitForm = function () {
        var data = {};
        var fields = document.getElementsByTagName('input');
        var selectFields = document.getElementsByTagName('select');
        for (var i = 0; i < fields.length; i++) {
            var key = fields[i].getAttribute('id');
            var value = fields[i].value;
            if (value == "")
                value = null;
            if (fields[i].type == "checkbox") {
                if (fields[i].checked) {
                    value = true;
                }
                else {
                    value = false;
                }
                
                console.log(data[key]);
            }

            data[key] = value;
        }

        for (var i = 0; i < selectFields.length; i++) {
            var key = selectFields[i].getAttribute('id');
            var value = selectFields[i].value;
            data[key] = value;
        }

        console.log(data);

        tableFactory.insertTable(uri, data)
            .then(function (response) {
            
                $scope.tableData.push(response.data);
                toastr.success("Uspešno upisivanje u bazu");
                var beforeState = $state.current.params.parent;
                $scope.trueFalse = false;
                $state.go(beforeState);
            }, function (error) {
                toastr.error("Greška pri upisivanju elementa u bazu");
            });
    };
          

    $scope.restore = function () {
        var beforeState = $state.current.params.parent;
        $scope.trueFalse = false;
        $state.go(beforeState, null);
    };

    $scope.getReport = function(data) {
        $scope.dataForReport = data;
        $state.go($state.current.name + '.izvestaj');
    }

    $scope.prikaziStavke = function (data) {
        if ($state.current.name.includes('stavke')) {
            $state.go($state.current.params.parent)
                .then(function () {
                    $state.go('.stavke');
                    $scope.dokument = data;
                });
        }
        else {
            $state.go(".stavke");
            $scope.dokument = data;
        }
    }

    $scope.cenaValue = { value: 0 };
    $scope.kolValue = {value : 0};
   
    $scope.next = function (nextTable, data) {

        var filterData = {};

        if (code == 'PROMETNI_DOKUMENT' && nextTable.Code == 'STAVKE_DOKUMENTA') {
            filterData = {
                id: $scope.PrimaryKey,
                value: data[$scope.PrimaryKey],
                dokument: data
            }
        }
        else {
            filterData = {
                id: $scope.PrimaryKey,
                value: data[$scope.PrimaryKey]
            }
        }

        var currentState = {
            name: $state.current.name,
            params: $state.params
        };

        $rootScope.statesChain.push(currentState);
        
        $state.go(nextTable.Uri, { isNext: true, filter: filterData });
    }

    $scope.nextFilter = function (data) {
        if (!$scope.isNext)
            return true;
        return data[$scope.filter.id] == $scope.filter.value;
    }

    $scope.goBack = function () {
        var state = $rootScope.statesChain[$rootScope.statesChain.length - 1];
        $rootScope.statesChain.pop();
        $state.go(state.name, state.params);
    }

    $scope.getSelectOptions = function (columnCode) {
        var retVal = [];
        for(var key in $scope.zoomValues[columnCode]) {
            var data = {
                Id: key,
                Value : $scope.zoomValues[columnCode][key]
            }
            retVal.push(data);
        }

        return retVal;
    }

    $scope.getSelectedOption = function () {
        var options = $scope.getSelectOptions($scope.filter.id);
        for (var i = 0; i < options.length; i++) {
            if (options[i].Id == $scope.filter.value)
                return options[i];
        }
    }

    $scope.zoom = function (columnCode) {
        var tableUri;
        for (var i = 0; i < $scope.zoomTables.length; i++)
            if ($scope.zoomTables[i].ColumnCode == columnCode) {
                tableUri = $scope.zoomTables[i].Table.Uri;
            }

        var currentState = {
            name: $state.current.name,
            params: $state.params
        };

        $rootScope.statesChain.push(currentState);

        $state.go(tableUri, {isZoom: true, isNext: false, filter: null });
    }

    $scope.robaChanged = function () {
        console.log("changed");
        var inputRoba = document.getElementById("ID_ROBE");
        var idRobe = inputRoba.value;
        var inputVrstaDokumenta = document.getElementById("ID_DOKUMENTA");
        var vrstaDokumenta = inputVrstaDokumenta.options[inputVrstaDokumenta.selectedIndex].text;
        
        if (vrstaDokumenta == "Ot" || vrstaDokumenta == "Mm") {
            tableFactory.getCenaRobe(idRobe)
                .then(function (response) {
                    $scope.cenaValue.value = response.data;
                }, function (error) {
                    console.log(error.message);
                });
        }

    }

    $scope.callProcedure = function (data, uri, procedure, $event) {

        var dataToSend = {};

        if (code == 'PROMETNI_DOKUMENT') {
            dataToSend = {
                procedure: procedure,
                data: data
            }
        }
        else if (code == 'MAGACIN') {
            dataToSend = {
                procedure: procedure,
                data: data,
                id_godine: $scope.godinaPoslovanjaID,
                godina: $rootScope.godinaPoslovanja
            }
        }

        tableFactory.callProcedure(uri, dataToSend)
            .then(function (response) {
                toastr.success("Procedura uspešno izvršena");

                /*if (procedure == 'Storno') {
                    for (var i = 0; i < $scope.tableData.length; i++) {
                        if ($scope.tableData[i].ID_DOKUMENTA == data.ID_DOKUMENTA) {
                            $scope.tableData[i].STATUS = 'S';
                            $event.currentTarget.disabled = true;
                        }
                    }
                }

                if (procedure == 'Pred_knjizenje') {
                    for (var i = 0; i < $scope.tableData.length; i++) {
                        if ($scope.tableData[i].ID_DOKUMENTA == data.ID_DOKUMENTA) {
                            $scope.tableData[i].STATUS = 'P';
                            $event.currentTarget.disabled = true;
                        }
                    }
                }*/

                $state.go($state.current.name, null, { reload: true });

            }, function (error) {
                toastr.error("Greška pri izvršavanju procedure. " + error.data.Message);
            });
    } 

}


var routingModule = angular.module('routingModule');
routingModule.controller('routingController', ['$scope', '$state', '$stateParams', '$rootScope', '$timeout', 'tableFactory', 'metadataFactory', '$uibModal',  routingController]);