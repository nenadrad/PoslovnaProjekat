﻿searchController = function ($scope, $state, $stateParams, $http, tableFactory) {

    $scope.submitFormSearch = function () {
        var data = {};
        var fields = document.getElementsByTagName('input');
        var selectFields = document.getElementsByTagName('select');
        for (var i = 0; i < fields.length; i++) {
            var key = fields[i].getAttribute('id');
            var value = fields[i].value;
            if (fields[i].type == "checkbox") {
                if (value == "on")
                    value = true;
                if (value == "off")
                    value = false;
            }
            data[key] = value;
        }

        for (var i = 0; i < selectFields.length; i++) {
            var key = selectFields[i].getAttribute('id');
            var value = selectFields[i].value;
            if (value == '?')
                value = "";
            data[key] = value;
        }

       // console.log(data);

        var dataToSend = {
            TableCode: $scope.tableCode,
            Query: JSON.stringify(data)
        }

        var searchData = [];

        $http.post("/api/search", dataToSend)
            .then(function (response) {
                console.log(response);
                $scope.tableData.splice(0, $scope.tableData.length);
                for (var i = 0; i < response.data.length; i++) {
                    var obj = JSON.parse(response.data[i]);
                    console.log(obj);
                    searchData.push(obj);
                    $scope.tableData.push(obj);
                }
                console.log(searchData);
                console.log($scope.tableData);

            }, function (error) {
                console.log(error.message);
            });
    };

    $scope.restoreSearch = function () {
        var beforeState = $state.current.params.parent;
        $scope.trueFalse = false;
        $state.go(beforeState, null, {reload: true});
    };

    $scope.provera = function () {
        var result = true;
        var status = false;
        var fields = document.getElementsByTagName('input');
        for (var i = 0; i < fields.length; i++) {
            var key = fields[i].getAttribute('id');
            var value = fields[i].value;
            if (value)
                status = true
        }

        var selectFields = document.getElementsByTagName('select');        
        for (var i = 0; i < selectFields.length; i++) {
            var key = selectFields[i].getAttribute('id');
            var value = selectFields[i].value;
            if (value == '?')
                value = "";
            console.log(key+" : "+ value);
            if (value!='')
                status = true
        }

        return !status;
    };
}

var tableModule = angular.module('tableModule');
tableModule.controller('searchController', ['$scope', '$state', '$stateParams', '$http', 'tableFactory', searchController]);