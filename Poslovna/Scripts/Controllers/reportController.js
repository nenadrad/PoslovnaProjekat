﻿reportController = function ($scope, $sce, $http) {
    $scope.test = "angular test";
    $scope.item = $sce.trustAsHtml("<h1>IncludeTest</h1>");

    authenticate().then(function (response) {
        console.log("uspesno logovanje");
        $http.get("http://localhost:8080/jasperserver/rest_v2/reports/Reports/MestoReport.html")
            .then(function (response) {
                console.log("uspesno ucitan izvestaj");
                $scope.item = $sce.trustAsHtml(response.data);
                console.log(response.data);
            }, function (error) {
                console.log(error);
            });

    }, function (error) {
        console.log(error);
    });

    function authenticate() {

        var url = "http://localhost:8080/jasperserver/rest/login?j_username=jasperadmin&j_password=jasperadmin";

        return $http.get(url);
    }
}

var app = angular.module("app");
app.controller('reportController', ['$scope', '$sce', '$http', reportController]);