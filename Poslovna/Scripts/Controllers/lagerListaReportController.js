﻿lagerListaReportController = function ($scope, $http, $sce) {

    var data = $scope.dataForReport;

    $scope.showButton = true;

    function authenticate() {

        var url = "http://localhost:8080/jasperserver/rest/login?j_username=jasperadmin&j_password=jasperadmin";

        return $http.get(url);
    }

    authenticate().then(function (response) {
        console.log("uspesno logovanje");
        console.log(data.ID_MAGACINA + "&ime_magacina=" + data.NAZIV_MAGACINA + "&id_godine=" + $scope.godinaPoslovanjaID);
        $http.get("http://localhost:8080/jasperserver/rest_v2/reports/Reports/LagerLista.html?magacin=" + data.ID_MAGACINA + "&ime_magacina=" + data.NAZIV_MAGACINA + "&id_godine=" + $scope.godinaPoslovanjaID)
            .then(function (response) {
            console.log("uspesno ucitan izvestaj");
            $scope.reportHtml = $sce.trustAsHtml(response.data);
            console.log(response.data);
        }, function (error) {
            console.log(error);
        });

    }, function (error) {
        console.log(error);
    });

}


var app = angular.module('app');
app.controller('lagerListaReportController', ['$scope', '$http', '$sce', lagerListaReportController]);