﻿var metadataFactory = function ($http) {
    var urlTableNames = '/api/metadata';
    var tableMetadata = {};

    tableMetadata.getTableNames = function () {
        return $http.get(urlTableNames);
    }

    tableMetadata.getTableColums = function (id) {
        return $http.get(urlTableNames+"/"+id);
    }

    tableMetadata.getPrimaryKey = function (id) {
        return $http.get("api/primarykey/" + id);
    }

    tableMetadata.getNextTables = function (id) {
        return $http.get("api/next/" + id);
    }

    tableMetadata.getZoomTables = function (id) {
        return $http.get("api/zoom/" + id);
    }
    
    return tableMetadata;

};


var metadataModule = angular.module('metadataModule');
metadataModule.factory('metadataFactory', metadataFactory);