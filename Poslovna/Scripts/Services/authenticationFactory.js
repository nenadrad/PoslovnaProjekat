﻿var authenticationFactory = function ($http, $state, $location, $stateParams, tableFactory, $localStorage) {
    var service = {};

    service.login = login;
    service.logout = logout;
    service.register = register;
    service.getCurrentUser = getCurrentUser;

    return service;

    function login(username, password, callback) {
        var uri = $stateParams["tableToLoadUrl"];
        var korisnik = {}
        $http.get('/api/Korisnik' + "/" + 1)
            .success(function (response) {
                console.log(response.token);
                
                korisnik = response;
                if (korisnik.KORISNICKO_IME == username && korisnik.SIFRA == password) {
                    $localStorage.currentUser = korisnik;
                    $http.defaults.headers.common.Authorization = response.token;

                    toastr.success("Dobrodosli " + korisnik.IME);
                    callback(true);
                    $state.go('Home');
                }
            }, function (error) {
                toastr.error("Korisnicko ime ili sifra nisu ispravni");
                callback(false);
            });


    }

    function register() {
        console.log("saasas");
        $state.go('register');
    }

    function logout() {
        // uklonimo korisnika iz lokalnog skladišta
        delete $localStorage.currentUser;
        $http.defaults.headers.common.Authorization = '';
        $state.go('login');
    }

    function getCurrentUser() {
        return $localStorage.currentUser;
    }
}

var loginModule = angular.module('loginModule');
loginModule.factory('authenticationFactory', authenticationFactory);