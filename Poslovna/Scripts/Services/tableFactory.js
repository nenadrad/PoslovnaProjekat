﻿var tableFactory = function ($http) {

    var urlBase = '/api';
    var dataFactory = {};

    dataFactory.getTable = function (uri) {
        return $http.get(urlBase + uri);
    };

    dataFactory.getItem = function (uri, id) {
        return $http.get(urlBase + uri + "/" + id);
    }

    dataFactory.postLogin = function (uri, id) {
        return $http.get(urlBase + uri + "/" + id);
    }

    dataFactory.getPoslovnaGodina = function () {
        return $http.get(urlBase + "/PoslovnaGodina");
    }

    dataFactory.insertTable = function (uri, item) {
        return $http.post(urlBase+uri, item);
    };

    dataFactory.updateTable = function (uri, id, item) {
        console.log(uri + " - " + id);
        console.log(item);
        return $http.put(urlBase + uri + '/' + id, item)
    };

    dataFactory.deleteTable = function (uri, id) {
        return $http.delete(urlBase + uri+ "/" + id);
    };

    dataFactory.getStavke = function (id) {
        return $http.get("/api/stavke/" + id);
    };

    dataFactory.getZoomValues = function(data) {
        return $http.post("/api/zoomvalues", data);
    }

    dataFactory.getCenaRobe = function (idRobe) {
        return $http.get("api/cena/" + idRobe);
    }

    dataFactory.callProcedure = function (uri, data) {
        return $http.post("api/" + uri, data);
    }

    return dataFactory;
}
var tableModule = angular.module('tableModule');
tableModule.factory('tableFactory', tableFactory);