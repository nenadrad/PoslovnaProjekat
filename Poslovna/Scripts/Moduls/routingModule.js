﻿var application = angular.module("app");

application.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    //$urlRouterProvider.otherwise('/Home');

    $stateProvider
    .state('login', {
        url: '/Login',
        templateUrl: 'home/loginForm',
        controller: 'loginController',
        params: {
            tableToLoadUrl: "/Korisnik",
        }
    })
    .state('register', {
        url: '/Register',
        templateUrl: 'home/registerForm',
        controller: 'registerController',
    })

    .state('Home', {
        url: '/Home',       
    })
        
    .state('AnalitikaMagacinskeKartice', {
        url: '/AnalitikaMagacinskeKartice',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "ANALITIKA_MAGACINSKE_KARTICE",
            tableToLoadUrl: "/AnalitikaMagacinskeKartice",
            tableName: "Analitika magacinske kartice",
            isNext: false,
            filter: null,
            isZoom : false
        }
    })
        .state('AnalitikaMagacinskeKartice.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'AnalitikaMagacinskeKartice',
            }
        })
        .state('AnalitikaMagacinskeKartice.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'AnalitikaMagacinskeKartice',
                isBackFromZoom: false,
                editData : null
            }
        })
        .state('AnalitikaMagacinskeKartice.pretraga', {
            url: '/pretraga',
            templateUrl: '/home/formtemplatesearch',
            controller: 'searchController',
            params: {
                parent: 'AnalitikaMagacinskeKartice',
            }
        })
    .state('GrupaRoba', {
        url: '/GrupaRoba',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "GRUPA_ROBA",
            tableToLoadUrl: "/GrupaRoba",
            tableName: "Grupa roba",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('GrupaRoba.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'GrupaRoba',
            }
        })
        .state('GrupaRoba.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'GrupaRoba',
                isBackFromZoom: false,
                editData: null
            }
        })
        .state('GrupaRoba.pretraga', {
            url: '/pretraga',
            templateUrl: '/home/formtemplatesearch',
            controller: 'searchController',
            params: {
                parent: 'GrupaRoba',
            }})

        
    .state('JedinicaMere', {
        url: '/JedinicaMere',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "JEDINICA_MERE",
            tableToLoadUrl: "/JedinicaMere",
            tableName: "Jedinica mere",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('JedinicaMere.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'JedinicaMere',
            }
 
        })
        .state('JedinicaMere.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'JedinicaMere',
                isBackFromZoom: false,
                editData: null
            }

        })
        .state('JedinicaMere.pretraga', {
            url: '/pretraga',
            templateUrl: '/home/formtemplatesearch',
            controller: 'searchController',
            params: {
                parent: 'JedinicaMere',
            }

        })
      
    .state('Magacin', {
        url: '/Magacin',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "MAGACIN",
            tableToLoadUrl: "/Magacin",
            tableName: "Magacin",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('Magacin.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'Magacin',
            }
        })
        .state('Magacin.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'Magacin',
                isBackFromZoom: false,
                editData: null
            }
        })
    .state('Magacin.pretraga', {
        url: '/pretraga',
        templateUrl: '/home/formtemplatesearch',
        controller: 'searchController',
        params: {
            parent: 'Magacin',
        }
    })
    .state('Magacin.izvestaj', {
        url: '/izvestaj',
        templateUrl: '/home/reportTemplate',
        controller: 'lagerListaReportController',
        params: {
            parent: 'Magacin',
        }})

    .state('Mesto', {
        url: '/Mesto',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "MESTO",
            tableToLoadUrl: "/Mesto",
            tableName: "Mesto",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('Mesto.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'Mesto',
            }
        })
        .state('Mesto.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'Mesto',
                isBackFromZoom: false,
                editData: null
            }
        })
        .state('Mesto.next', {
            url: '/next',
            templateUrl: '/home/nexttemplate',
            controller: 'nextController',
            params: {
                parent: 'Mesto',
            }
        })
        .state('Mesto.pretraga', {
            url: '/pretraga',
            templateUrl: '/home/formtemplatesearch',
            controller: 'searchController',
            params: {
                parent: 'Mesto',
            }})
    .state('PoslovnaGodina', {
        url: '/PoslovnaGodina',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "POSLOVNA_GODINA",
            tableToLoadUrl: "/PoslovnaGodina",
            tableName: "Poslovna godina",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('PoslovnaGodina.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'PoslovnaGodina',
            }
        })
        .state('PoslovnaGodina.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'PoslovnaGodina',
                isBackFromZoom: false,
                editData: null
            }
        })
        .state('PoslovnaGodina.pretraga', {
            url: '/pretraga',
            templateUrl: '/home/formtemplatesearch',
            controller: 'searchController',
            params: {
                parent: 'PoslovnaGodina',
            }})
    .state('PoslovniPartner', {
        url: '/PoslovniPartner',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "POSLOVNI_PARTNER",
            tableToLoadUrl: "/PoslovniPartner",
            tableName: "Poslovni partner",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('PoslovniPartner.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'PoslovniPartner',
            }
        })
        .state('PoslovniPartner.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'PoslovniPartner',
                isBackFromZoom: false,
                editData: null
            }
        })
        .state('PoslovniPartner.pretraga', {
            url: '/pretraga',
            templateUrl: '/home/formtemplatesearch',
            controller: 'searchController',
            params: {
                parent: 'PoslovniPartner',
            }})
    .state('Preduzece', {
        url: '/Preduzece',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "PREDUZECE",
            tableToLoadUrl: "/Preduzece",
            tableName: "Preduzece",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('Preduzece.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'Preduzece',
            }
        })
        .state('Preduzece.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'Preduzece',
                isBackFromZoom: false,
                editData: null
            }
        })
    .state('Preduzece.pretraga', {
        url: '/pretraga',
        templateUrl: '/home/formtemplatesearch',
        controller: 'searchController',
        params: {
            parent: 'Preduzece',
        }})
    .state('PrometniDokument', {
        url: '/PrometniDokument',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "PROMETNI_DOKUMENT",
            tableToLoadUrl: "/PrometniDokument",
            tableName: "Prometni dokument",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('PrometniDokument.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'PrometniDokument',
            }
        })
        .state('PrometniDokument.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'PrometniDokument',
                isBackFromZoom: false,
                editData: null
            }
        })
    .state('PrometniDokument.pretraga', {
        url: '/pretraga',
        templateUrl: '/home/formtemplatesearch',
        controller: 'searchController',
        params: {
            parent: 'PrometniDokument',
        }
    })
    .state('PrometniDokument.stavke', {
        url: '/stavke',
        templateUrl: '/home/stavkedokumentatemplate',
        controller: 'stavkeDokumentaController',
        params: {
            parent: 'PrometniDokument',
        }
    })
    .state('PrometniDokument.stavke.izmena', {
        url: '/stavke',
        templateUrl: '/home/formtemplateedit',
        controller: 'editController',
        params: {
            parent: 'PrometniDokument.stavke',
        }
    })
    .state('Radnik', {
        url: '/Radnik',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "RADNIK",
            tableToLoadUrl: "/Radnik",
            tableName: "Radnik",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('Radnik.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'Radnik',
            }
        })
        .state('Radnik.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'Radnik',
                isBackFromZoom: false,
                editData: null
            }
        })
    .state('Radnik.pretraga', {
        url: '/pretraga',
        templateUrl: '/home/formtemplatesearch',
        controller: 'searchController',
        params: {
            parent: 'Radnik',
        }})
    .state('Roba', {
        url: '/Roba',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "ROBA",
            tableToLoadUrl: "/Roba",
            tableName: "Roba",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('Roba.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'Roba',
            }
        })
        .state('Roba.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'Roba',
                isBackFromZoom: false,
                editData: null
            }
        })
      .state('Roba.pretraga', {
          url: '/pretraga',
          templateUrl: '/home/formtemplatesearch',
          controller: 'searchController',
          params: {
              parent: 'Roba',
          }})
    .state('RobnaKartica', {
        url: '/RobnaKartica',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "ROBNA_KARTICA",
            tableToLoadUrl: "/RobnaKartica",
            tableName: "Robna kartice",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('RobnaKartica.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'RobnaKartica',
            }
        })
        .state('RobnaKartica.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'RobnaKartica',
                isBackFromZoom: false,
                editData: null
            }
        })
    .state('RobnaKartica.pretraga', {
        url: '/pretraga',
        templateUrl: '/home/formtemplatesearch',
        controller: 'searchController',
        params: {
            parent: 'RobnaKartica',
        }
    })
    .state('RobnaKartica.izvestaj', {
        url: '/izvestaj',
        templateUrl: '/home/reportTemplate',
        controller: 'robnaKarticaReportController',
        params: {
            parent: 'RobnaKartica',
        }
    })
    .state('StavkeDokumenta', {
        url: '/StavkeDokumenta',
        templateUrl: '/home/tabletemplate',
        controller: 'routingController',
        params: {
            tableCode: "STAVKE_DOKUMENTA",
            tableToLoadUrl: "/StavkeDokumenta",
            tableName: "Stavke dokumenta",
            isNext: false,
            filter: null,
            isZoom: false
        }
    })
        .state('StavkeDokumenta.dodavanje', {
            url: '/dodavanje',
            templateUrl: '/home/formtemplate',
            params: {
                parent: 'StavkeDokumenta',
            }
        })
        .state('StavkeDokumenta.izmena', {
            url: '/izmena',
            templateUrl: '/home/formtemplateedit',
            controller: 'editController',
            params: {
                parent: 'StavkeDokumenta',
                isBackFromZoom: false,
                editData: null
            }
        })
        .state('StavkeDokumenta.pretraga', {
            url: '/pretraga',
            templateUrl: '/home/formtemplatesearch',
            controller: 'searchController',
            params: {
                parent: 'StavkeDokumenta',
            }})
        }]);