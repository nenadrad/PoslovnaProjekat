﻿using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Poslovna.Controllers
{
    public class StavkeController : ApiController
    {

        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<STAVKE_DOKUMENTA> repository = null;

        public StavkeController()
        {
            repository = unitOfWork.StavkeDokumentaRepository;
        }

        // GET: api/Stavke
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Stavke/5
        public IEnumerable<STAVKE_DOKUMENTA> Get(int id)
        {
            var result = repository.Get().Where(s => s.ID_DOKUMENTA == id).ToArray<STAVKE_DOKUMENTA>();
            return result;
        }

        // POST: api/Stavke
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Stavke/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Stavke/5
        public void Delete(int id)
        {
        }
    }
}
