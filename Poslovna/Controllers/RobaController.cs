﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;
using System.Text;

namespace Poslovna.Controllers
{
    public class RobaController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<ROBA> repository = null;

        public RobaController() 
        {
            repository = unitOfWork.RobaRepository;
        }

        // GET api/Roba
        public IEnumerable<ROBA> GetROBAs()
        {
            return repository.GetAll();
        }

        // GET api/Roba/5
        [ResponseType(typeof(ROBA))]
        public IHttpActionResult GetROBA(int id)
        {
            ROBA roba = repository.GetByID(id);
            if (roba == null)
            {
                return NotFound();
            }

            return Ok(roba);
        }

        // PUT api/Roba/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutROBA(int id, ROBA roba)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != roba.ID_ROBE)
            {
                return BadRequest();
            }

            repository.Update(roba);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Roba
        [ResponseType(typeof(ROBA))]
        public IHttpActionResult PostROBA(ROBA roba)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ROBA p = repository.GetByID(roba.ID_ROBE);
            if (p != null)
            {
                return BadRequest();
            }

            repository.Insert(roba);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = roba.ID_ROBE }, roba);
        }

        // DELETE api/Roba/5
        [ResponseType(typeof(ROBA))]
        public IHttpActionResult DeleteROBA(int id)
        {
            ROBA roba = repository.GetByID(id);
            if (roba == null)
            {
                return NotFound();
            }

            repository.Delete(roba);
            try
            {
                unitOfWork.Save();
                return Ok(roba);

            } catch (DbUpdateException ex)
            { 
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;
                  
                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                 return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));
                
            }
         }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ROBAExists(int id)
        {
            return repository.Exists(id);
        }
    }
}