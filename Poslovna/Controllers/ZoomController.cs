﻿using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Poslovna.Controllers
{
    public class ZoomController : ApiController
    {

        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: api/Zoom
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Zoom/5
        public IEnumerable<FKModel> Get(string id)
        {
            IEnumerable <FKModel> retVal = unitOfWork.GetParentTables(id);
            setLookupColumns(retVal);
            return retVal;
        }

        // POST: api/Zoom
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Zoom/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Zoom/5
        public void Delete(int id)
        {
        }

        private void setLookupColumns(IEnumerable<FKModel> retVal)
        {
            foreach(FKModel value in retVal)
            {
                switch(value.ColumnCode)
                {
                    case "ID_MESTA":
                        value.LookupColumnCode = "NAZIV_MESTA";
                        break;
                    case "ID_GRUPE":
                        value.LookupColumnCode = "NAZIV_GRUPE";
                        break;
                    case "ID_JEDINICE":
                        value.LookupColumnCode = "NAZIV_JEDINICE_MERE";
                        break;
                    case "ID_DOKUMENTA":
                        value.LookupColumnCode = "VRSTA_DOKUMENTA";
                        break;
                    case "ID_ROBE":
                        value.LookupColumnCode = "NAZIV_ROBE";
                        break;
                    case "ID_PREDUZECA":
                        value.LookupColumnCode = "NAZIV_PREDUZECA";
                        break;
                    case "ID_MAGACINA":
                        value.LookupColumnCode = "NAZIV_MAGACINA";
                        break;
                    case "MAG_ID_MAGACINA":
                        value.LookupColumnCode = "NAZIV_MAGACINA";
                        break;
                    case "ID_GODINE":
                        value.LookupColumnCode = "GODINA";
                        break;
                    case "ID_PARTNERA":
                        value.LookupColumnCode = "NAZIV_PARTNERA";
                        break;
                    case "ID_KARTICE":
                        value.LookupColumnCode = "ID_KARTICE";
                        break;
                    default:
                        value.LookupColumnCode = "default";
                        break;
                }
            }
        }
    }
}
