﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class PreduzeceController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<PREDUZECE> repository = null;

        public PreduzeceController() 
        {
            repository = unitOfWork.PreduzeceRepository;
        }

        // GET api/Preduzece
        public IEnumerable<PREDUZECE> GetPREDUZECEs()
        {
            return repository.GetAll();
        }

        // GET api/Preduzece/5
        [ResponseType(typeof(PREDUZECE))]
        public IHttpActionResult GetPREDUZECE(int id)
        {
            PREDUZECE preduzece = repository.GetByID(id);
            if (preduzece == null)
            {
                return NotFound();
            }

            return Ok(preduzece);
        }

        // PUT api/Preduzece/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPREDUZECE(int id, PREDUZECE preduzece)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != preduzece.ID_PREDUZECA)
            {
                return BadRequest();
            }

            repository.Update(preduzece);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Preduzece
        [ResponseType(typeof(PREDUZECE))]
        public IHttpActionResult PostPREDUZECE(PREDUZECE preduzece)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            PREDUZECE p = repository.GetByID(preduzece.ID_PREDUZECA);
            if (p != null)
            {
                return BadRequest();
            }

            repository.Insert(preduzece);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = preduzece.ID_PREDUZECA }, preduzece);
        }

        // DELETE api/Preduzece/5
        [ResponseType(typeof(PREDUZECE))]
        public IHttpActionResult DeletePREDUZECE(int id)
        {
            PREDUZECE preduzece = repository.GetByID(id);
            if (preduzece == null)
            {
                return NotFound();
            }

            repository.Delete(preduzece);
            try
            {
                unitOfWork.Save();
                return Ok(preduzece);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PREDUZECEExists(int id)
        {
            return repository.Exists(id);
        }
    }
}