﻿using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Poslovna.Controllers
{
    public class DokumentProcedureController : ApiController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        public object ConnectionString { get; private set; }

        public class Model
        {
            public string procedure { get; set; }
            public PROMETNI_DOKUMENT data { get; set; }
        }

        // GET: api/DokumentProcedure
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/DokumentProcedure/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/DokumentProcedure
        public string Post(Model model)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = unitOfWork.ConnectionString;
            conn.Open();

            SqlDataReader reader;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.CommandText = model.procedure;
            cmd.Parameters.AddWithValue("@ID_DOKUMENTA", model.data.ID_DOKUMENTA);
            cmd.Parameters.AddWithValue("@VRSTA_DOKUMENTA", model.data.VRSTA_DOKUMENTA);
            cmd.Parameters.AddWithValue("OK", false);
            try
            { 
                reader = cmd.ExecuteReader();

                conn.Close();
                return "success";
            }
            catch(Exception e)
            {

                throw new HttpResponseException(
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));

            }
            
        }

        // PUT: api/DokumentProcedure/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/DokumentProcedure/5
        public void Delete(int id)
        {
        }
    }
}
