﻿using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Poslovna.Controllers
{
    public class CenaController : ApiController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        // GET: api/Cena
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Cena/5
        public decimal Get(int id)
        {
            int idRobe = id;
            var  cena = unitOfWork.RobnaKarticaRepository.Get().Where(r => r.ID_ROBE == idRobe).Select(p => p.PROSECNA_CENA).FirstOrDefault();
            if (cena == null)
                return 0;
            return (decimal)cena;
        }

        // POST: api/Cena
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Cena/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Cena/5
        public void Delete(int id)
        {
        }
    }
}
