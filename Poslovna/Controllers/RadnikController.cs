﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class RadnikController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<RADNIK> repository = null;

        public RadnikController() 
        {
            repository = unitOfWork.RadnikRepository;
        }

        // GET api/Radnik
        public IEnumerable<RADNIK> GetRADNIKs()
        {
            return repository.GetAll();
        }

        // GET api/Radnik/5
        [ResponseType(typeof(RADNIK))]
        public IHttpActionResult GetRADNIK(int id)
        {
            RADNIK radnik = repository.GetByID(id);
            if (radnik == null)
            {
                return NotFound();
            }

            return Ok(radnik);
        }

        // PUT api/Radnik/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRADNIK(int id, RADNIK radnik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != radnik.ID_RADNIKA)
            {
                return BadRequest();
            }

            repository.Update(radnik);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Radnik
        [ResponseType(typeof(RADNIK))]
        public IHttpActionResult PostRADNIK(RADNIK radnik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RADNIK p = repository.GetByID(radnik.ID_RADNIKA);
            if (p != null)
            {
                return BadRequest();
            }

            repository.Insert(radnik);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = radnik.ID_RADNIKA }, radnik);
        }

        // DELETE api/Radnik/5
        [ResponseType(typeof(RADNIK))]
        public IHttpActionResult DeleteRADNIK(int id)
        {
            RADNIK radnik = repository.GetByID(id);
            if (radnik == null)
            {
                return NotFound();
            }

            repository.Delete(radnik);
            try
            {
                unitOfWork.Save();
                return Ok(radnik);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RADNIKExists(int id)
        {
            return repository.Exists(id);
        }
    }
}