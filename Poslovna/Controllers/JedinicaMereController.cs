﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class JedinicaMereController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<JEDINICA_MERE> repository = null;

        public JedinicaMereController() 
        {
            repository = unitOfWork.JedinicaMereRepository;
        }

        // GET api/JeidnicaMere
        public IEnumerable<JEDINICA_MERE> GetJEIDNICA_MERE()
        {
            return repository.GetAll();
        }

        // GET api/JeidnicaMere/5
        [ResponseType(typeof(JEDINICA_MERE))]
        public IHttpActionResult GetJEIDNICA_MERE(int id)
        {
            JEDINICA_MERE jeidnica_mere = repository.GetByID(id);
            if (jeidnica_mere == null)
            {
                return NotFound();
            }

            return Ok(jeidnica_mere);
        }

        // PUT api/JeidnicaMere/5
        public IHttpActionResult PutJEIDNICA_MERE(int id, JEDINICA_MERE jeidnica_mere)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != jeidnica_mere.ID_JEDINICE)
            {
                return BadRequest();
            }

            repository.Update(jeidnica_mere);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/JeidnicaMere
        [ResponseType(typeof(JEDINICA_MERE))]
        public IHttpActionResult PostJEIDNICA_MERE(JEDINICA_MERE jeidnica_mere)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            JEDINICA_MERE jedinica = repository.GetByID(jeidnica_mere.ID_JEDINICE);
            if (jedinica!=null) 
            {
                return BadRequest();
               
            }
            repository.Insert(jeidnica_mere);
            unitOfWork.Save();
            return CreatedAtRoute("DefaultApi", new { id = jeidnica_mere.ID_JEDINICE }, jeidnica_mere);
            
        }

        // DELETE api/JeidnicaMere/5
        [ResponseType(typeof(JEDINICA_MERE))]
        public IHttpActionResult DeleteJEIDNICA_MERE(int id)
        {
            JEDINICA_MERE jeidnica_mere = repository.GetByID(id);
            if (jeidnica_mere == null)
            {
                return NotFound();
            }

           repository.Delete(jeidnica_mere);
            try
            {
                unitOfWork.Save();
                return Ok(jeidnica_mere);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JEIDNICA_MEREExists(int id)
        {
            return repository.Exists(id);
        }
    }
}