﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class KorisnikController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<KORISNIK> repository = null;


        public KorisnikController() 
        {
            repository = unitOfWork.KorisnikRepository;
        }
        // GET api/Korisnik
        public IEnumerable<KORISNIK> GetKORISNIK()
        {
            return repository.GetAll();
        }

        // GET api/Korisnik/5
        [ResponseType(typeof(KORISNIK))]
        public IHttpActionResult GetKORISNIK(int id)
        {
            KORISNIK korisnik = repository.GetByID(id);
            if (korisnik == null)
            {
                return NotFound();
            }

            return Ok(korisnik);
        }

        // PUT api/Korisnik/5
        public IHttpActionResult PutKORISNIK(int id, KORISNIK korisnik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != korisnik.ID_KORISNIKA)
            {
                return BadRequest();
            }
            repository.Update(korisnik);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Korisnik
        [ResponseType(typeof(KORISNIK))]
        public  IHttpActionResult PostKORISNIK(KORISNIK korisnik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            KORISNIK p = repository.GetByID(korisnik.ID_KORISNIKA);
            if (p != null) 
            {
                return BadRequest();
            }
           // repository.Insert(korisnik);
           // unitOfWork.Save();
            return Ok(p);//CreatedAtRoute("DefaultApi", new { id = korisnik.ID_KORISNIKA}, korisnik);
        }

        // DELETE api/Korisnik/5
        [ResponseType(typeof(KORISNIK))]
        public IHttpActionResult DeleteKORISNIK(int id)
        {
            KORISNIK korisnik = repository.GetByID(id);
            if (korisnik == null)
            {
                return NotFound();
            }

            repository.Delete(korisnik);
            try
            {
                unitOfWork.Save();
                return Ok(korisnik);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KORISNIKExists(int id)
        {
            return repository.Exists(id);
        }
    }
}