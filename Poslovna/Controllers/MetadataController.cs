﻿using Poslovna.Models;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Poslovna.Controllers
{
    public class MetadataController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: api/Metadata
        public IEnumerable<TableMetaModels> Get()
        {
            return unitOfWork.GetMetadata();
        }

        // GET: api/Metadata/5
        public IEnumerable<Column> Get(string id)
        {
            return unitOfWork.GetColumnsOfTable(id);
        }

    }
}
