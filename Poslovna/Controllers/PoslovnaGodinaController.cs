﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class PoslovnaGodinaController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<POSLOVNA_GODINA> repository = null;

        public PoslovnaGodinaController() 
        {
            repository = unitOfWork.PoslovnaGodinaRepository;
        }

        // GET api/PoslovnaGodina
        public IEnumerable<POSLOVNA_GODINA> GetPOSLOVNA_GODINA()
        {
            return repository.GetAll();
        }

        // GET api/PoslovnaGodina/5
        [ResponseType(typeof(POSLOVNA_GODINA))]
        public IHttpActionResult GetPOSLOVNA_GODINA(int id)
        {
            POSLOVNA_GODINA poslovna_godina = repository.GetByID(id);
            if (poslovna_godina == null)
            {
                return NotFound();
            }

            return Ok(poslovna_godina);
        }

        // PUT api/PoslovnaGodina/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPOSLOVNA_GODINA(int id, POSLOVNA_GODINA poslovna_godina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != poslovna_godina.ID_GODINE)
            {
                return BadRequest();
            }

            repository.Update(poslovna_godina);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/PoslovnaGodina
        [ResponseType(typeof(POSLOVNA_GODINA))]
        public IHttpActionResult PostPOSLOVNA_GODINA(POSLOVNA_GODINA poslovna_godina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            POSLOVNA_GODINA p = repository.GetByID(poslovna_godina.ID_GODINE);
            if (p != null)
            {
                return BadRequest();
            }

            repository.Insert(poslovna_godina);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = poslovna_godina.ID_GODINE }, poslovna_godina);
        }

        // DELETE api/PoslovnaGodina/5
        [ResponseType(typeof(POSLOVNA_GODINA))]
        public IHttpActionResult DeletePOSLOVNA_GODINA(int id)
        {
            POSLOVNA_GODINA poslovna_godina = repository.GetByID(id);
            if (poslovna_godina == null)
            {
                return NotFound();
            }

            repository.Delete(poslovna_godina);
            try
            {
                unitOfWork.Save();
                return Ok(poslovna_godina);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool POSLOVNA_GODINAExists(int id)
        {
            return repository.Exists(id);
        }
    }
}