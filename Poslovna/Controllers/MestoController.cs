﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class MestoController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<MESTO> repository = null;

        public MestoController()
        {
            repository = unitOfWork.MestoRepository;
        }

        // GET: api/Mesto
        public IEnumerable<MESTO> GetMESTO()
        {
            return repository.GetAll();
        }

        // GET: api/Mesto/5
        [ResponseType(typeof(MESTO))]
        public IHttpActionResult GetMESTO(int id)
        {
            MESTO item = repository.GetByID(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // PUT: api/Mesto/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMESTO(int id, MESTO item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.ID_MESTA)
            {
                return BadRequest();
            }


            repository.Update(item);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Mesto
        [ResponseType(typeof(MESTO))]
        public IHttpActionResult PostMESTO(MESTO item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            MESTO mesto = repository.GetByID(item.ID_MESTA);
            if (mesto != null)
            {
                return BadRequest();
            }

            repository.Insert(item);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = item.ID_MESTA }, item);
        }

        // DELETE: api/Mesto/5
        [ResponseType(typeof(MESTO))]
        public IHttpActionResult DeleteMESTO(int id)
        {
            MESTO item = repository.GetByID(id);
            if (item == null)
            {
                return NotFound();
            }

            /*db.MESTO.Remove(item);
            db.SaveChanges();*/

            repository.Delete(item);
            try
            {
                unitOfWork.Save();
                return Ok(item);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MESTOExists(int id)
        {
            //return db.MESTO.Count(e => e.ID_MESTA == id) > 0;
            return repository.Exists(id);
        }
    }
}