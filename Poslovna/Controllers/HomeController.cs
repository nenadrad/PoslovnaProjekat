﻿using PoslovnaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PoslovnaDAL.Repository;

namespace Poslovna.Controllers
{
    public class HomeController : Controller
    {
        private UnitOfWork work = new UnitOfWork();


        public ActionResult Index()
        {
            ViewBag.Title = "Poslovna informatika";
            return View();
        }

        public ActionResult TableTemplate()
        {
            return View("~/Views/Templates/TableTemplate.cshtml");
        }

        public ActionResult FormTemplate()
        {
            return View("~/Views/Templates/FormTemplate.cshtml");
        }

        public ActionResult FormTemplateEdit()
        {
            return View("~/Views/Templates/FormTemplateEdit.cshtml");
        }

        public ActionResult FormTemplateSearch() 
        {
            return View("~/Views/Templates/FormTemplateSearch.cshtml");
        }

        public ActionResult ReportTemplate()
        {
            return View("~/Views/Templates/ReportTemplate.cshtml");
        }

        public ActionResult StavkeDokumentaTemplate()
        {
            return View("~/Views/Templates/StavkeDokumentaTemplate.cshtml");
        }

        public ActionResult ReportTest()
        {
            return View();
        }
        public ActionResult LoginForm() 
        {
            return View("~/Views/Templates/LoginForm.cshtml");
        }
        public ActionResult RegisterForm() 
        {
            return View("~/Views/Templates/RegisterForm.cshtml");
        }
        public ActionResult PoslovnaGodinaForm()
        {
            return View("~/Views/Templates/PoslovnaGodinaForm.cshtml");
        }
        public ActionResult DeleteForm() {
            return View("~/Views/Templates/DeleteForm.cshtml");
        }
        public ActionResult NextTemplate()
        {
            return View("~/Views/Templates/NextTemplate.cshtml");
        }
    }
}
