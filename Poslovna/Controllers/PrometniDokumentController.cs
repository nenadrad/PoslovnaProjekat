﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class PrometniDokumentController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<PROMETNI_DOKUMENT> repository = null;

        public PrometniDokumentController() 
        {
            repository = unitOfWork.PrometniDokumentRepository;
        }

        // GET api/PrometniDokument
        public IEnumerable<PROMETNI_DOKUMENT> GetPROMETNI_DOKUMENT()
        {
            return repository.GetAll();
        }

        // GET api/PrometniDokument/5
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public IHttpActionResult GetPROMETNI_DOKUMENT(int id)
        {
            PROMETNI_DOKUMENT prometni_dokument = repository.GetByID(id);
            if (prometni_dokument == null)
            {
                return NotFound();
            }

            return Ok(prometni_dokument);
        }

        // PUT api/PrometniDokument/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPROMETNI_DOKUMENT(int id, PROMETNI_DOKUMENT prometni_dokument)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != prometni_dokument.ID_DOKUMENTA)
            {
                return BadRequest();
            }


            repository.Update(prometni_dokument);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/PrometniDokument
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public IHttpActionResult PostPROMETNI_DOKUMENT(PROMETNI_DOKUMENT prometni_dokument)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            PROMETNI_DOKUMENT p = repository.GetByID(prometni_dokument.ID_DOKUMENTA);
            if (p != null)
            {
                return BadRequest();
            }

            repository.Insert(prometni_dokument);
            unitOfWork.Save();
            return CreatedAtRoute("DefaultApi", new { id = prometni_dokument.ID_DOKUMENTA }, prometni_dokument);
        }

        // DELETE api/PrometniDokument/5
        [ResponseType(typeof(PROMETNI_DOKUMENT))]
        public IHttpActionResult DeletePROMETNI_DOKUMENT(int id)
        {
            PROMETNI_DOKUMENT prometni_dokument = repository.GetByID(id);
            if (prometni_dokument == null)
            {
                return NotFound();
            }

            repository.Delete(prometni_dokument);
            try
            {
                unitOfWork.Save();
                return Ok(prometni_dokument);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PROMETNI_DOKUMENTExists(int id)
        {
            return repository.Exists(id);
        }
    }
}