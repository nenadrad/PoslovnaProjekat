﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class GrupaRobaController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<GRUPA_ROBA> repository = null;

        public GrupaRobaController() 
        {
            repository = unitOfWork.GrupaRobaRepository;
        }

        // GET: api/GrupaRoba
        public IEnumerable<GRUPA_ROBA> GetGRUPA_ROBA()
        {
            return repository.GetAll();
        }

        // GET: api/GrupaRoba/5
        [ResponseType(typeof(GRUPA_ROBA))]
        public IHttpActionResult GetGRUPA_ROBA(int id)
        {
            GRUPA_ROBA item = repository.GetByID(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // PUT: api/GrupaRoba/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGRUPA_ROBA(int id, GRUPA_ROBA item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.ID_GRUPE)
            {
                return BadRequest();
            }

            repository.Update(item);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GrupaRoba
        [ResponseType(typeof(GRUPA_ROBA))]
        public IHttpActionResult PostGRUPA_ROBA(GRUPA_ROBA item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            GRUPA_ROBA grupa_roba = repository.GetByID(item.ID_GRUPE);
            if (grupa_roba != null)
            {
                return BadRequest();
            }

            repository.Insert(item);
            unitOfWork.Save();
            return CreatedAtRoute("DefaultApi", new { id = item.ID_GRUPE }, item);
        }

        // DELETE: api/GrupaRoba/5
        [ResponseType(typeof(GRUPA_ROBA))]
        public IHttpActionResult DeleteGRUPA_ROBA(int id)
        {
            GRUPA_ROBA item = repository.GetByID(id);
            if (item == null)
            {
                return NotFound();
            }


            repository.Delete(item);
            try
            {
                unitOfWork.Save();
                return Ok(item);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GRUPA_ROBAExists(int id)
        {
            return repository.Exists(id);
        }
    }
}