﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class PoslovniPartnerController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<POSLOVNI_PARTNER> repository = null;

        public PoslovniPartnerController() 
        {
            repository = unitOfWork.PoslovniPartnerRepository;
        }

        // GET api/PoslovniPartner
        public IEnumerable<POSLOVNI_PARTNER> GetPOSLOVNI_PARTNER()
        {
            return repository.GetAll();
        }

        // GET api/PoslovniPartner/5
        [ResponseType(typeof(POSLOVNI_PARTNER))]
        public IHttpActionResult GetPOSLOVNI_PARTNER(int id)
        {
            POSLOVNI_PARTNER poslovni_partner = repository.GetByID(id);
            if (poslovni_partner == null)
            {
                return NotFound();
            }

            return Ok(poslovni_partner);
        }

        // PUT api/PoslovniPartner/5
        [ResponseType(typeof(void))]
        public  IHttpActionResult PutPOSLOVNI_PARTNER(int id, POSLOVNI_PARTNER poslovni_partner)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != poslovni_partner.ID_PARTNERA)
            {
                return BadRequest();
            }

            repository.Update(poslovni_partner);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/PoslovniPartner
        [ResponseType(typeof(POSLOVNI_PARTNER))]
        public IHttpActionResult PostPOSLOVNI_PARTNER(POSLOVNI_PARTNER poslovni_partner)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            POSLOVNI_PARTNER p = repository.GetByID(poslovni_partner.ID_PARTNERA);
            if (p != null)
            {
                return BadRequest();
            }

            repository.Insert(poslovni_partner);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = poslovni_partner.ID_PARTNERA }, poslovni_partner);
        }

        // DELETE api/PoslovniPartner/5
        [ResponseType(typeof(POSLOVNI_PARTNER))]
        public  IHttpActionResult DeletePOSLOVNI_PARTNER(int id)
        {
            POSLOVNI_PARTNER poslovni_partner = repository.GetByID(id);
            if (poslovni_partner == null)
            {
                return NotFound();
            }

            repository.Delete(poslovni_partner);
            try
            {
                unitOfWork.Save();
                return Ok(poslovni_partner);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool POSLOVNI_PARTNERExists(int id)
        {
            return repository.Exists(id);
        }
    }
}