﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class StavkeDokumentaController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<STAVKE_DOKUMENTA> repository = null;

        public StavkeDokumentaController() 
        {
            repository = unitOfWork.StavkeDokumentaRepository;
        }

        // GET api/StavkeDokumenta
        public IEnumerable<STAVKE_DOKUMENTA> GetSTAVKE_DOIKUMENTA()
        {
            return repository.GetAll();
        }

        // GET api/StavkeDokumenta/5
        [ResponseType(typeof(STAVKE_DOKUMENTA))]
        public IHttpActionResult GetSTAVKE_DOIKUMENTA(int id)
        {
            STAVKE_DOKUMENTA stavke_doikumenta = repository.GetByID(id);
            if (stavke_doikumenta == null)
            {
                return NotFound();
            }

            return Ok(stavke_doikumenta);
        }

        // PUT api/StavkeDokumenta/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSTAVKE_DOIKUMENTA(int id, STAVKE_DOKUMENTA stavke_doikumenta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != stavke_doikumenta.ID_STAVKE_DOKUMENTA)
            {
                return BadRequest();
            }

            repository.Update(stavke_doikumenta);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/StavkeDokumenta
        [ResponseType(typeof(STAVKE_DOKUMENTA))]
        public IHttpActionResult PostSTAVKE_DOIKUMENTA(STAVKE_DOKUMENTA stavke_doikumenta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            STAVKE_DOKUMENTA p = repository.GetByID(stavke_doikumenta.ID_STAVKE_DOKUMENTA);
            if (p != null)
            {
                return BadRequest();
            }
            repository.Insert(stavke_doikumenta);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = stavke_doikumenta.ID_STAVKE_DOKUMENTA }, stavke_doikumenta);
        }

        // DELETE api/StavkeDokumenta/5
        [ResponseType(typeof(STAVKE_DOKUMENTA))]
        public IHttpActionResult DeleteSTAVKE_DOIKUMENTA(int id)
        {
            STAVKE_DOKUMENTA stavke_doikumenta = repository.GetByID(id);
            if (stavke_doikumenta == null)
            {
                return NotFound();
            }

            repository.Delete(stavke_doikumenta);
            try
            {
                unitOfWork.Save();
                return Ok(stavke_doikumenta);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool STAVKE_DOIKUMENTAExists(int id)
        {
            return repository.Exists(id);
        }


        [Route("api/getstavke")]
        public IEnumerable<STAVKE_DOKUMENTA> GetStavke(int idDok)
        {
            var result = repository.Get().Where(s => s.ID_DOKUMENTA == idDok).ToArray<STAVKE_DOKUMENTA>();
            return result;         
        }






    }
}