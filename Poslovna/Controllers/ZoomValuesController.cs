﻿using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Poslovna.Controllers
{
    public class ZoomValuesController : ApiController
    {

        public class ZoomValuesModel
        {
            public string TableCode { get; set; }
            public FKModel FKModel { get; set; }
        }

        public class RetvalModel
        {
            public string Id { get; set; }
            public string Value { get; set; }
        }

        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: api/ZoomValues
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ZoomValues/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ZoomValues
        public IEnumerable<RetvalModel> Post(ZoomValuesModel model)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = unitOfWork.ConnectionString;
            conn.Open();

            SqlDataReader reader;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            if (model.FKModel.ColumnCode == "MAG_ID_MAGACINA")
            {
                //cmd.CommandText = "SELECT " + model.FKModel.LookupColumnCode + ", " + model.FKModel.Table.Code + "." + "ID_MAGACINA" + " FROM " + model.TableCode + " JOIN " + model.FKModel.Table.Code + " ON " + model.TableCode + "." + model.FKModel.ColumnCode + "=" + model.FKModel.Table.Code + "." + "ID_MAGACINA";
                cmd.CommandText = "SELECT NAZIV_MAGACINA, MAGACIN.ID_MAGACINA FROM MAGACIN";
            }
            else if(model.FKModel.ColumnCode == "ID_KARTICE")
            {
                //cmd.CommandText = "SELECT ROBA.NAZIV_ROBE, ROBNA_KARTICA.ID_KARTICE FROM ROBA, ROBNA_KARTICA, ANALITIKA_MAGACINSKE_KARTICE WHERE ROBA.ID_ROBE = ROBNA_KARTICA.ID_ROBE AND ROBNA_KARTICA.ID_KARTICE = ANALITIKA_MAGACINSKE_KARTICE.ID_KARTICE";
                cmd.CommandText = "SELECT ROBA.NAZIV_ROBE, ROBNA_KARTICA.ID_KARTICE FROM ROBA, ROBNA_KARTICA, ANALITIKA_MAGACINSKE_KARTICE WHERE ROBA.ID_ROBE = ROBNA_KARTICA.ID_ROBE";
            }
            else
            {
                //cmd.CommandText = "SELECT " + model.FKModel.LookupColumnCode + ", " + model.FKModel.Table.Code + "." + model.FKModel.ColumnCode + " FROM " + model.TableCode + " JOIN " + model.FKModel.Table.Code + " ON " + model.TableCode + "." + model.FKModel.ColumnCode + "=" + model.FKModel.Table.Code + "." + model.FKModel.ColumnCode;
                cmd.CommandText = "SELECT " + model.FKModel.LookupColumnCode + ", " + model.FKModel.ColumnCode + " FROM " + model.FKModel.Table.Code;
            }
            reader = cmd.ExecuteReader();

            List<RetvalModel> results = new List<RetvalModel>();

            while(reader.Read())
            {
                RetvalModel res = new RetvalModel();
                res.Id = reader.GetValue(1).ToString();
                res.Value = reader.GetValue(0).ToString();
                results.Add(res);
            }

            return results;
        }

        // PUT: api/ZoomValues/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ZoomValues/5
        public void Delete(int id)
        {
        }
    }
}
