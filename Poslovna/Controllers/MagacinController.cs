﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class MagacinController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<MAGACIN> repository = null;

        public MagacinController() 
        {
            repository = unitOfWork.MagacinRepository;
        }

        // GET api/Magacin
        public IEnumerable<MAGACIN> GetMAGACINs()
        {
            return repository.GetAll();
        }

        // GET api/Magacin/5
        [ResponseType(typeof(MAGACIN))]
        public IHttpActionResult GetMAGACIN(int id)
        {
            MAGACIN magacin = repository.GetByID(id);
            if (magacin == null)
            {
                return NotFound();
            }

            return Ok(magacin);
        }

        // PUT api/Magacin/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMAGACIN(int id, MAGACIN magacin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != magacin.ID_MAGACINA)
            {
                return BadRequest();
            }

            repository.Update(magacin);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Magacin
        [ResponseType(typeof(MAGACIN))]
        public IHttpActionResult PostMAGACIN(MAGACIN magacin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            MAGACIN mag = repository.GetByID(magacin.ID_MAGACINA);
            if (mag != null)
            {
                return BadRequest();
            }

            repository.Insert(magacin);
            unitOfWork.Save();
            return CreatedAtRoute("DefaultApi", new { id = magacin.ID_MAGACINA }, magacin);
        }

        // DELETE api/Magacin/5
        [ResponseType(typeof(MAGACIN))]
        public IHttpActionResult DeleteMAGACIN(int id)
        {
            MAGACIN magacin = repository.GetByID(id);
            if (magacin == null)
            {
                return NotFound();
            }

            repository.Delete(id);
            try
            {
                unitOfWork.Save();
                return Ok(magacin);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MAGACINExists(int id)
        {
            return repository.Exists(id);
        }
    }
}