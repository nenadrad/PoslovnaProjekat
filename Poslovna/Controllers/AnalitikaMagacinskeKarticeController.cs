﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class AnalitikaMagacinskeKarticeController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<ANALITIKA_MAGACINSKE_KARTICE> repository = null;

        public AnalitikaMagacinskeKarticeController()
        {
            repository = unitOfWork.AnalitikaMagacinskeKarticeRepository;
        }

        // GET api/AnalitikaMagacinskeKartice
        public IEnumerable<ANALITIKA_MAGACINSKE_KARTICE> GetAnalitikaMagacinskeKartice()
        {
            return repository.GetAll();
        }

        // GET api/AnalitikaMagacinskeKartice/5
        [ResponseType(typeof(ANALITIKA_MAGACINSKE_KARTICE))]
        public IHttpActionResult GetAnalitikaMagacinskeKartice(int id)
        {
            ANALITIKA_MAGACINSKE_KARTICE item = repository.GetByID(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }


        // PUT api/AnalitikaMagacinskeKartice/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutANALITIKA_MAGACINSKE_KARTICE(int id, ANALITIKA_MAGACINSKE_KARTICE item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.ID_KARTICE)
            {
                return BadRequest();
            }

            repository.Update(item);
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }



        // POST api/AnalitikaMagacinskeKartice
        [ResponseType(typeof(ANALITIKA_MAGACINSKE_KARTICE))]
        public IHttpActionResult PostANALITIKA_MAGACINSKE_KARTICE(ANALITIKA_MAGACINSKE_KARTICE item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ANALITIKA_MAGACINSKE_KARTICE analitika = repository.GetByID(item.ID_ANALITIKE);
            if (analitika != null) 
            {
                return BadRequest();
            }
            repository.Insert(item);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = item.ID_KARTICE }, item);

        }

        // DELETE api/AnalitikaMagacinskeKartice/5
        [ResponseType(typeof(ANALITIKA_MAGACINSKE_KARTICE))]
        public IHttpActionResult DeleteANALITIKA_MAGACINSKE_KARTICE(int id)
        {
            ANALITIKA_MAGACINSKE_KARTICE item = repository.GetByID(id);
            if (item == null)
            {
                return NotFound();
            }
            repository.Delete(item);
            try
            {
                unitOfWork.Save();
                return Ok(item);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ANALITIKA_MAGACINSKE_KARTICEExists(int id)
        {
            return repository.Exists(id);
        }

    }
}