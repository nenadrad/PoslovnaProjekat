﻿using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Poslovna.Controllers
{
    public class NextController : ApiController
    {

        private UnitOfWork unitOfWork = new UnitOfWork();

        // GET: api/Next
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Next/5
        public IEnumerable<TableMetaModels> Get(string id)
        {
            return unitOfWork.GetNextTables(id);
        }

        // POST: api/Next
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Next/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Next/5
        public void Delete(int id)
        {
        }
    }
}
