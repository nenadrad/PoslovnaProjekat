﻿using Newtonsoft.Json;
using Poslovna.Models.Search;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Poslovna.Controllers
{
    public class SearchController : ApiController
    {
        // POST: api/Search
        public IEnumerable<string> Post(SearchData data)
        {
            UnitOfWork unitOfWork = new UnitOfWork();

            var codesValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data.Query);
            List<Column> columns = (List<Column>)unitOfWork.GetColumnsOfTable(data.TableCode);

            string sqlSearch = "SELECT * FROM " + data.TableCode + " WHERE ";
            string conditions = "";
            foreach(Column col in columns)
            {
                if (!col.IsPrimary)
                {
                    string value = codesValues[col.Code];
                    if (value != "")
                    {
                        if (col.Type.Equals("char") || col.Type.Equals("varchar"))
                        {
                            conditions += col.Code + " LIKE '%" + value + "%' AND ";
                        }
                        if (col.Type.Equals("int") || col.Type.Equals("decimal") || col.Type.Equals("numeric"))
                        {
                            conditions += col.Code + "=" + value + " AND ";
                        }
                        if (col.Type.Equals("date") || col.Type.Equals("datetime"))
                        {
                            conditions += col.Code + "='" + value + "' AND ";
                        }
                    }
                }
            }


            sqlSearch += conditions.Substring(0, conditions.Length-5);
            Console.WriteLine(sqlSearch);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = unitOfWork.ConnectionString;
            conn.Open();

            SqlDataReader reader;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sqlSearch;
            reader = cmd.ExecuteReader();

            List<string> results = new List<string>();

            while(reader.Read())
            {
                var item = new Dictionary<string, string>();
                foreach(string code in codesValues.Keys)
                {
                    item[code] = reader[code].ToString();
                }
                string serItem = JsonConvert.SerializeObject(item);
                results.Add(serItem);
            }

            conn.Close();

            return results;

            
        }

    }
}
