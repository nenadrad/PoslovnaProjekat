﻿using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Poslovna.Controllers
{
    public class MagacinProcedureController : ApiController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        public class Model
        {
            public string procedure { get; set; }
            public MAGACIN data { get; set; }
            public int id_godine { get; set; }
            public int godina { get; set; }
            
        }

        // GET: api/MagacinProcedure
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/MagacinProcedure/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/MagacinProcedure
        public string Post(Model model)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = unitOfWork.ConnectionString;
            conn.Open();

            SqlDataReader reader;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.CommandText = model.procedure;

            if(model.procedure=="Nivelacija")
            {
                cmd.Parameters.AddWithValue("@ID_MAGACINA", model.data.ID_MAGACINA);
                cmd.Parameters.AddWithValue("@ID_GODINE", model.id_godine);
                cmd.Parameters.AddWithValue("OK", false);
            }
            else if (model.procedure == "KreiranjeKartice")
            {
                cmd.Parameters.AddWithValue("@ID_MAGACINA", model.data.ID_MAGACINA);
                cmd.Parameters.AddWithValue("@ID_GODINE", model.id_godine);
                cmd.Parameters.AddWithValue("OK", false);
                cmd.Parameters.AddWithValue("@GODINA", model.godina);
                cmd.Parameters.AddWithValue("@ID_PREDUZECA", model.data.ID_PREDUZECA);
            }
            try {

                reader = cmd.ExecuteReader();
                conn.Close();
                return "success";
            }catch(Exception e)
            {
                throw new HttpResponseException(
                   Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        // PUT: api/MagacinProcedure/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/MagacinProcedure/5
        public void Delete(int id)
        {
        }
    }
}
