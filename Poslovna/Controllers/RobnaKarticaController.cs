﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PoslovnaDAL.Entities;
using PoslovnaDAL.Repository;
using System.Data.SqlClient;

namespace Poslovna.Controllers
{
    public class RobnaKarticaController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        private GenericRepository<ROBNA_KARTICA> repository = null;

        public RobnaKarticaController() 
        {
            repository = unitOfWork.RobnaKarticaRepository;
        }

        // GET api/RobnaKartica
        public IEnumerable<ROBNA_KARTICA> GetROBNA_KARTICA()
        {
            return repository.GetAll();
        }

        // GET api/RobnaKartica/5
        [ResponseType(typeof(ROBNA_KARTICA))]
        public IHttpActionResult GetROBNA_KARTICA(int id)
        {
            ROBNA_KARTICA robna_kartica = repository.GetByID(id);
            if (robna_kartica == null)
            {
                return NotFound();
            }

            return Ok(robna_kartica);
        }

        // PUT api/RobnaKartica/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutROBNA_KARTICA(int id, ROBNA_KARTICA robna_kartica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != robna_kartica.ID_KARTICE)
            {
                return BadRequest();
            }

            repository.Update(robna_kartica);
            unitOfWork.Save();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/RobnaKartica
        [ResponseType(typeof(ROBNA_KARTICA))]
        public IHttpActionResult PostROBNA_KARTICA(ROBNA_KARTICA robna_kartica)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ROBNA_KARTICA p = repository.GetByID(robna_kartica.ID_KARTICE);
            if (p != null)
            {
                return BadRequest();
            }
            repository.Insert(robna_kartica);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = robna_kartica.ID_KARTICE }, robna_kartica);
        }

        // DELETE api/RobnaKartica/5
        [ResponseType(typeof(ROBNA_KARTICA))]
        public IHttpActionResult DeleteROBNA_KARTICA(int id)
        {
            ROBNA_KARTICA robna_kartica = repository.GetByID(id);
            if (robna_kartica == null)
            {
                return NotFound();
            }

            repository.Delete(robna_kartica);
            try
            {
                unitOfWork.Save();
                return Ok(robna_kartica);

            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Nije moguce izbrisati element s ovim id-em"));

                    }

                }

                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "Greska prilikom brisanja ovog elementa"));

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ROBNA_KARTICAExists(int id)
        {
            return repository.Exists(id);
        }
    }
}