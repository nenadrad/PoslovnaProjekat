﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Poslovna.Models.Search
{
    public class SearchData
    {
        public string TableCode { get; set; }
        public string Query { get; set; }
    }
}