﻿using PoslovnaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoslovnaDAL.Repository
{
    public class UnitOfWork
    : IDisposable
    {
        #region Private member variables...

        private PoslovnaProjekatEntities _context = null;
        private GenericRepository<MESTO> _mestoRepository;
        private GenericRepository<ANALITIKA_MAGACINSKE_KARTICE> _analitikaMagacinskeKarticeRepository;
        private GenericRepository<GRUPA_ROBA> _grupaRobaRepository;
        private GenericRepository<JEDINICA_MERE> _jedinicaMereRepository;
        private GenericRepository<MAGACIN> _magacinRepository;
        private GenericRepository<POSLOVNA_GODINA> _poslovnaGodinaRepository;
        private GenericRepository<POSLOVNI_PARTNER> _poslovniPartnerRepository;
        private GenericRepository<PREDUZECE> _preduzeceRepository;
        private GenericRepository<PROMETNI_DOKUMENT> _prometniDokumentRepository;
        private GenericRepository<RADNIK> _radnikRepository;
        private GenericRepository<ROBA> _robaRepository;
        private GenericRepository<ROBNA_KARTICA> _robnaKarticaRepository;
        private GenericRepository<STAVKE_DOKUMENTA> _stavkeDokumentaRepository;
        private GenericRepository<KORISNIK> _korisnikRepository;

        #endregion

        public UnitOfWork()
        {
            _context = new PoslovnaProjekatEntities();
        }

        #region Public Repository Creation properties...

        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>

        public GenericRepository<KORISNIK> KorisnikRepository
        {
            get
            {
                if (this._korisnikRepository == null)
                    this._korisnikRepository = new GenericRepository<KORISNIK>(_context);
                return _korisnikRepository;
            }
        }




        public GenericRepository<MESTO> MestoRepository
        {
            get
            {
                if (this._mestoRepository == null)
                    this._mestoRepository = new GenericRepository<MESTO>(_context);
                return _mestoRepository;
            }
        }

        public GenericRepository<ANALITIKA_MAGACINSKE_KARTICE> AnalitikaMagacinskeKarticeRepository
        {
            get
            {
                if (this._analitikaMagacinskeKarticeRepository == null)
                    this._analitikaMagacinskeKarticeRepository = new GenericRepository<ANALITIKA_MAGACINSKE_KARTICE>(_context);
                return _analitikaMagacinskeKarticeRepository;
            }
        }

        public GenericRepository<GRUPA_ROBA> GrupaRobaRepository
        {
            get
            {
                if (this._grupaRobaRepository == null)
                    this._grupaRobaRepository = new GenericRepository<GRUPA_ROBA>(_context);
                return _grupaRobaRepository;
            }
        }

        public GenericRepository<JEDINICA_MERE> JedinicaMereRepository
        {
            get
            {
                if (this._jedinicaMereRepository == null)
                    this._jedinicaMereRepository = new GenericRepository<JEDINICA_MERE>(_context);
                return _jedinicaMereRepository;
            }
        }

        public GenericRepository<MAGACIN> MagacinRepository
        {
            get
            {
                if (this._magacinRepository == null)
                    this._magacinRepository = new GenericRepository<MAGACIN>(_context);
                return _magacinRepository;
            }
        }

        public GenericRepository<POSLOVNA_GODINA> PoslovnaGodinaRepository
        {
            get
            {
                if (this._poslovnaGodinaRepository == null)
                    this._poslovnaGodinaRepository = new GenericRepository<POSLOVNA_GODINA>(_context);
                return _poslovnaGodinaRepository;
            }
        }

        public GenericRepository<POSLOVNI_PARTNER> PoslovniPartnerRepository
        {
            get
            {
                if (this._poslovniPartnerRepository == null)
                    this._poslovniPartnerRepository = new GenericRepository<POSLOVNI_PARTNER>(_context);
                return _poslovniPartnerRepository;
            }
        }

        public GenericRepository<PREDUZECE> PreduzeceRepository
        {
            get
            {
                if (this._preduzeceRepository == null)
                    this._preduzeceRepository = new GenericRepository<PREDUZECE>(_context);
                return _preduzeceRepository;
            }
        }

        public GenericRepository<PROMETNI_DOKUMENT> PrometniDokumentRepository
        {
            get
            {
                if (this._prometniDokumentRepository == null)
                    this._prometniDokumentRepository = new GenericRepository<PROMETNI_DOKUMENT>(_context);
                return _prometniDokumentRepository;
            }
        }

        public GenericRepository<RADNIK> RadnikRepository
        {
            get
            {
                if (this._radnikRepository == null)
                    this._radnikRepository = new GenericRepository<RADNIK>(_context);
                return _radnikRepository;
            }
        }

        public GenericRepository<ROBA> RobaRepository
        {
            get
            {
                if (this._robaRepository == null)
                    this._robaRepository = new GenericRepository<ROBA>(_context);
                return _robaRepository;
            }
        }

        public GenericRepository<ROBNA_KARTICA> RobnaKarticaRepository
        {
            get
            {
                if (this._robnaKarticaRepository == null)
                    this._robnaKarticaRepository = new GenericRepository<ROBNA_KARTICA>(_context);
                return _robnaKarticaRepository;
            }
        }

        public GenericRepository<STAVKE_DOKUMENTA> StavkeDokumentaRepository
        {
            get
            {
                if (this._stavkeDokumentaRepository == null)
                    this._stavkeDokumentaRepository = new GenericRepository<STAVKE_DOKUMENTA>(_context);
                return _stavkeDokumentaRepository;
            }
        }

        #endregion

        #region Public member methods...
        /// <summary>
        /// Save method.
        /// </summary>
        public void Save() 
        {
            try
            {

                   _context.SaveChanges(); 

               
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"C:\errors.txt", outputLines);

                throw e;
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                if (sqlException != null)
                {
                    var number = sqlException.Number;

                    if (number == 547)
                    {
                        Console.WriteLine("Must delete products before deleting category");
                    }
                }
                throw ex;
            }

        }

        public string ConnectionString
        {
            get
            {
                return _context.Database.Connection.ConnectionString;
            }
        }

        #endregion

        #region metadata public methods

        public IEnumerable<TableMetaModels> GetMetadata()
        {
            var metadata = ((IObjectContextAdapter)_context).ObjectContext.MetadataWorkspace;
            var tableNames = new List<TableMetaModels>();

            var tables = metadata.GetItemCollection(DataSpace.SSpace)
                .GetItems<EntityContainer>()
                .Single()
                .BaseEntitySets
                .OfType<EntitySet>()
                .Where(s => !s.MetadataProperties.Contains("Type")
                || s.MetadataProperties["Type"].ToString() == "Tables");

            foreach (var table in tables)
            {
                var tableName = table.MetadataProperties.Contains("Table")
                    && table.MetadataProperties["Table"].Value != null
                    ? table.MetadataProperties["Table"].Value.ToString()
                    : table.Name;

                tableNames.Add(new TableMetaModels(tableName));
            }
            return tableNames;
        }

        public IEnumerable<Column> GetColumnsOfTable(string tableName)
        {
            var metadata = ((IObjectContextAdapter)_context).ObjectContext.MetadataWorkspace;
            var columnNames = new List<Column>();

            var table = metadata.GetItemCollection(DataSpace.SSpace)
                .GetItems<EntityContainer>()
                .Single()
                .BaseEntitySets
                .OfType<EntitySet>()
                .Where(s => (!s.MetadataProperties.Contains("Type") || s.MetadataProperties["Type"].ToString() == "Tables") && s.Name.Equals(tableName)).FirstOrDefault();

            IEnumerable<string> primaryKeyNames = table.ElementType.KeyMembers.Select(k => k.Name).ToArray();

            IEnumerable<EdmProperty> columns = table.ElementType.Properties.ToArray();
            foreach (EdmProperty column in columns)
            {
                bool isPrimary = primaryKeyNames.Contains(column.Name);
                columnNames.Add(new Column(column, isPrimary));
            }


            return columnNames;
        }

        public IEnumerable<TableMetaModels> GetNextTables(string tableName)
        {
            List<TableMetaModels> properties = new List<TableMetaModels>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConnectionString;
            conn.Open();

            SqlDataReader reader;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "EXEC sp_fkeys'" + tableName + "'";
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                properties.Add(new TableMetaModels(reader.GetString(6)));
            }

            conn.Close();

            return properties;
        }

        public IEnumerable<FKModel> GetParentTables(string tableName)
        {
            List<FKModel> properties = new List<FKModel>();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConnectionString;
            conn.Open();

            SqlDataReader reader;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "ImportedKeysTable";
            cmd.Parameters.AddWithValue("@TABLE_NAME", tableName);
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                //properties.Add(new TableMetaModels(reader.GetString(3)));
                TableMetaModels table = new TableMetaModels(reader.GetString(3));
                string columnCode = reader.GetString(2);
                FKModel fk = new FKModel();
                fk.Table = table;
                fk.ColumnCode = columnCode;
                properties.Add(fk);
            }

            conn.Close();

            return properties;
        }

        #endregion

        #region Implementing IDiosposable...

        #region private dispose variable declaration...
        private bool disposed = false;
        #endregion

        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
