//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PoslovnaDAL.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class RADNIK
    {
        public int ID_RADNIKA { get; set; }
        public int ID_PREDUZECA { get; set; }
        public int ID_MAGACINA { get; set; }
        public string IME { get; set; }
        public string PREZIME { get; set; }
        public string BROJ_TELEFONA_RANDIKA { get; set; }
        public string ADRESA_RADNIKA { get; set; }
    
        public virtual MAGACIN MAGACIN { get; set; }
        public virtual PREDUZECE PREDUZECE { get; set; }
    }
}
