﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoslovnaDAL.Entities
{
    public class Column
    {
        private string code;
        private string uri;
        private string name;
        private string type;
        private bool isMandatory;
        private bool isPrimary;
        private bool isReadonly;
        private bool isFK;
        private string lookupColumnCode;
      
        public string Code { get { return code; } set { code = value; } }
        public string Uri { get { return uri; } set { uri = value; } }
        public string Name { get { return name; } set { name = value; } }
        public string Type { get { return type; } set { type = value; } }
        public bool IsMandatory { get { return isMandatory; } set { isMandatory = value; } }
        public bool IsPrimary { get { return isPrimary; } set { isPrimary = value; } }
        public bool IsFK { get { return isFK; } set { isFK = value; } }
        public string LookupColumnCode { get { return lookupColumnCode; } set { lookupColumnCode = value; } }
     
        public Column(EdmProperty column, bool isPrimary)
        {
            this.code = column.Name;
            this.uri = ToUri(code);
            this.name = ToName(code);
            this.type = GetType(column);
            this.isMandatory = column.Nullable;
            this.isPrimary = isPrimary;
            this.isFK = false;
        }


        private string ToUri(string code)
        {
            string retUri = "";
            string[] splitedCodes = code.Split('_');
            for (int i = 0; i < splitedCodes.Length; i++)
            {
                string splitedCode = splitedCodes[i];
                retUri += (splitedCode[0] + splitedCode.Substring(1).ToLower());
            }
            return retUri;
        }

        private string ToName(string code)
        {
            string retName = "";
            string replacedCode = code.Replace('_', ' ');

            retName = replacedCode[0] + replacedCode.Substring(1).ToLower();
            return retName;
        }

        private string GetType(EdmProperty column)
        {
            return column.TypeName;
        }
    }
}
