﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoslovnaDAL.Entities
{
    public class FKModel
    {
        public TableMetaModels Table { get; set; }
        public string ColumnCode { get; set; }
        public string LookupColumnCode { get; set; }
    }
}
