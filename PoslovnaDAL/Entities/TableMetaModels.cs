﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoslovnaDAL.Entities
{
    public class TableMetaModels
    {
        private string code;
        private string uri;
        private string name;
        private string primaryKey;
        
        public string Code { get { return code; } set { code = value; } }
        public string Uri { get { return uri; } set { uri = value; } }
        public string Name { get { return name; } set { name = value; } }
       
        public TableMetaModels(string code)
        {
            this.code = code;
            this.uri = ToUri(code);
            this.name = ToName(code);
        }


        private string ToUri(string code)
        {
            string retUri = "";
            string[] splitedCodes = code.Split('_');
            for (int i = 0; i < splitedCodes.Length; i++)
            {
                string splitedCode = splitedCodes[i];
                retUri += (splitedCode[0] + splitedCode.Substring(1).ToLower());
            }
            return retUri;
        }

        private string ToName(string code)
        {
            string retName = "";
            string replacedCode = code.Replace('_', ' ');

            retName = replacedCode[0] + replacedCode.Substring(1).ToLower();
            return retName;
        }

    }
}
