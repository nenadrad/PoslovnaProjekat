//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PoslovnaDAL.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class POSLOVNA_GODINA
    {
        public int ID_GODINE { get; set; }
        public int ID_PREDUZECA { get; set; }
        public decimal GODINA { get; set; }
        public bool ZAKLJUCENA { get; set; }
    
        public virtual PREDUZECE PREDUZECE { get; set; }
    }
}
